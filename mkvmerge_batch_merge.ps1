# mkvmerge_batch.ps1
# Batch merge subtitles into .mkv video containers
# Kai Sackl (cannonball1911), 09.02.2021

$mkvMergePath = "C:\Tools\MKVToolNix\mkvmerge.exe"
$ffmpegPath = "C:\Tools\ffmpeg-4.3.2\bin\ffmpeg.exe"
$videoFileExtension = "mkv"

$videoFiles = Get-ChildItem $PSScriptRoot -Filter *.$videoFileExtension

do {
    Write-Host "Remove all audio tracks expect one (1)"
    Write-Host "Remove all audio tracks expect one and remove all subtitle tracks (2)"
    Write-Host "Remove all audio tracks expect one and convert that audio track into another codec / change bitrate of audio track (3)"
    Write-Host "Remove all subtitle tracks expect one (4)"
    Write-Host "Remove all subtitle tracks and merge external subtitle file into mkv file (5)"
    Write-Host "Remove all subtitle tracks(6)"
	Write-Host "Remove all audio tracks expect one and remove all subtitle tracks expect one (7)"
    Write-Host "Remove all audio tracks and merge external audio file into mkv file (8)"
    $choice = Read-Host -Prompt "(1/2/3/4/5/6/7/8)"
    Write-Host ""
} until ($choice -eq "1" -or $choice -eq "2" -or $choice -eq "3" -or $choice -eq "4" -or $choice -eq "5" -or $choice -eq "6" -or $choice -eq "7" -or $choice -eq "8")

if ($choice -eq "1") {
    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        # List all TrackIDs for all (video) input files
        & $mkvMergePath --identify $videoFiles[$i]
    }

    Write-Host ""
    $trackID = Read-Host -Prompt "Which trackID do you want to keep?"

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - ONE-AUDIO-TRACK.$videoFileExtension"

        # Remove all audio tracks expect audio track with given ID
        & $mkvMergePath --output $newVideoFilePath --audio-tracks $trackID $videoFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED REMOVING ALL AUDIO TRACKS EXPECT ONE-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "2") {
    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        # List all TrackIDs for all (video) input files
        & $mkvMergePath --identify $videoFiles[$i]
    }

    Write-Host ""
    $trackID = Read-Host -Prompt "Which trackID do you want to keep?"

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - ONE-AUDIO-TRACK-NO-SUBTITLES.$videoFileExtension"

        # Remove all audio tracks expect audio track with given ID and remove all subtitle tracks
        & $mkvMergePath --no-subtitles --output $newVideoFilePath --audio-tracks $trackID $videoFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED REMOVING ALL AUDIO TRACKS EXPECT ONE AND REMOVE ALL SUBTITLE TRACKS-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "3") {
    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        # List all TrackIDs for all (video) input files
        & $mkvMergePath --identify $videoFiles[$i]
    }

    Write-Host ""
    $trackID = Read-Host -Prompt "Which trackID do you want to keep?"
    $codec = Read-Host -Prompt "Which audio codec (ffmpeg audio codec) do you want to convert into? (copy = Keep original codec, aac, ac3, eac3, ...)"
    $bitrate = Read-Host -Prompt "What bitrate do you want to use? (copy = Keep original bitrate, 640K = 640 kbit/s, 1M = 1 Mbit/s, ...)"

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - ONE-AUDIO-TRACK-AND-CONVERTED.$videoFileExtension"

        if ($codec -eq "copy" -and $bitrate -ne "copy") {
            & $ffmpegPath -i $videoFiles[$i] -c:v copy -c:s copy -b:a $bitrate -map 0:0 -map 0:$trackID -map 0:s $newVideoFilePath
        } elseif ($bitrate -eq "copy" -and $codec -ne "copy") {
            & $ffmpegPath -i $videoFiles[$i] -c:v copy -c:s copy -c:a $codec -map 0:0 -map 0:$trackID -map 0:s $newVideoFilePath
        } else {
            & $ffmpegPath -i $videoFiles[$i] -c:v copy -c:s copy -c:a $codec -b:a $bitrate -map 0:0 -map 0:$trackID -map 0:s $newVideoFilePath
        }
    }

    Write-Host ""
    Write-Host "-----FINISHED REMOVING ALL AUDIO TRACKS EXPECT ONE AND CONVERTING INTO ANOTHER CODEC/CHANGE BITRATE-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "4") {
    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        # List all TrackIDs for all (video) input files
        & $mkvMergePath --identify $videoFiles[$i]
    }

    Write-Host ""
    $trackID = Read-Host -Prompt "Which trackID do you want to keep?"

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - ONE-SUBTITLE-TRACK.$videoFileExtension"

        # Remove all subtitle tracks expect subtitle track with given ID
        & $mkvMergePath --output $newVideoFilePath --subtitle-tracks $trackID $videoFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED REMOVING ALL SUBTITLE TRACKS EXPECT ONE-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "5") {
    $subtitleFileExtension = Read-Host -Prompt "Which file extensions are the audio files? (sup, ass, ...)"
    $subtitleFiles = Get-ChildItem $PSScriptRoot -Filter *.$subtitleFileExtension

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $count = $i + 1
        Write-Host "($count) " -NoNewline
        Write-Host $subtitleFiles[$i].BaseName -ForegroundColor "Yellow" -NoNewline
        Write-Host " will be merged into " -NoNewline
        Write-Host $videoFiles[$i].BaseName -ForegroundColor "Red"
    }

    do {
        $answer = Read-Host -Prompt "Start merging? (Y/N)"
    } until ($answer -eq "Y" -or $answer -eq "y" -or $answer -eq "N" -or $answer -eq "n")

    if ($answer -eq "N" -or $answer -eq "n") { exit }

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - MERGED-SUBS.$videoFileExtension"

        # Remove all subtitles and add new subtitle on subtitle track pos 0, with no compression, as default track and german track name and language code
        & $mkvMergePath --no-subtitles --output $newVideoFilePath $videoFiles[$i] --default-track "0" --language "0:ger" --track-name "0:Deutsch Full" --compression "0:none" $subtitleFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED MERGING SUBTITLES INTO MKV-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "6") {
    do {
        $answer = Read-Host -Prompt "Start removing all subtitles? (Y/N)"
    } until ($answer -eq "Y" -or $answer -eq "y" -or $answer -eq "N" -or $answer -eq "n")

    if ($answer -eq "N" -or $answer -eq "n") { exit }

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - NO-SUBTITLES.$videoFileExtension"

        # Remove all subtitles
        & $mkvMergePath --no-subtitles --output $newVideoFilePath $videoFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED REMOVING ALL SUBTITLES-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "7") {
    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        # List all TrackIDs for all (video) input files
        & $mkvMergePath --identify $videoFiles[$i]
    }

    Write-Host ""
    $trackIDAudio = Read-Host -Prompt "Which audio trackID do you want to keep?"
	$trackIDSubtitle = Read-Host -Prompt "Which subtitle trackID do you want to keep?"

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - ONE-AUDIO-TRACK-ONE-SUBTITLE-TRACK.$videoFileExtension"

        # Remove all audio tracks expect audio track with given ID and remove all subtitle tracks expect subtitle track with given ID
        & $mkvMergePath --output $newVideoFilePath --audio-tracks $trackIDAudio --subtitle-tracks $trackIDSubtitle $videoFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED REMOVING ALL AUDIO TRACKS EXPECT ONE AND REMOVE ALL SUBTITLE TRACKS EXPECT ONE-----" -ForegroundColor "Green"
    Write-Host ""
} elseif ($choice -eq "8") {
    $audioFileExtension = Read-Host -Prompt "Which file extensions are the audio files? (mp3, ac3, ...)"
    $audioFiles = Get-ChildItem $PSScriptRoot -Filter *.$audioFileExtension

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $count = $i + 1
        Write-Host "($count) " -NoNewline
        Write-Host $audioFiles[$i].BaseName -ForegroundColor "Yellow" -NoNewline
        Write-Host " will be merged into " -NoNewline
        Write-Host $videoFiles[$i].BaseName -ForegroundColor "Red"
    }

    do {
        $answer = Read-Host -Prompt "Start merging? (Y/N)"
    } until ($answer -eq "Y" -or $answer -eq "y" -or $answer -eq "N" -or $answer -eq "n")

    if ($answer -eq "N" -or $answer -eq "n") { exit }

    for ($i = 0; $i -lt $videoFiles.Count; $i++) {
        $videoFileBaseName = $videoFiles[$i].BaseName
        $newVideoFilePath = Join-Path -Path $PSScriptRoot -ChildPath "$videofileBaseName - MERGED-AUDIO.$videoFileExtension"

        # Remove all audio tracks and add new audio on audio track
        & $mkvMergePath --no-audio --output $newVideoFilePath $videoFiles[$i] $audioFiles[$i]
    }

    Write-Host ""
    Write-Host "-----FINISHED MERGING AUDIO INTO MKV-----" -ForegroundColor "Green"
    Write-Host ""
}

Pause